<?php

namespace App\Tests\Integration;

use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;


class ApiControllerTest extends WebTestCase
{


    public function testValidUser()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "email" : "valid@email.com",
                "lname" : "testname",
                "fname" : "testname",
                "password" : "testesttest",
                "birthday" : "20-10-1990"
            }'
        );
        $this->assertResponseStatusCodeSame(201, $client->getResponse()->getStatusCode());
        $this->assertContains('{"status":"ok"', $client->getResponse()->getContent());
    }

    public function testInvalidUserMail()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "email" : "invalidemail.com",
                "lname" : "test",
                "fname" : "test",
                "password" : "12345678",
                "birthday" : "20-10-1990"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"Mail is not Valid !"}', $client->getResponse()->getContent());
    }

    public function testInvalidUserLastName()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "email" : "valid@email.com",
                "lname" : "",
                "fname" : "test",
                "password" : "12345678",
                "birthday" : "20-10-1990"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"Last Name is not valid !"}', $client->getResponse()->getContent());
    }

    public function testInvalidUserFirstName()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "email" : "valid@email.com",
                "lname" : "test",
                "fname" : "",
                "password" : "12345678",
                "birthday" : "20-10-1990"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"First Name is not valid !"}', $client->getResponse()->getContent());
    }

    public function testInvalidUserPassword()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "email" : "valid@email.com",
                "lname" : "test",
                "fname" : "test",
                "password" : "1234567",
                "birthday" : "20-10-1990"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"Password must be greater than 8 characters !"}', $client->getResponse()->getContent());
    }

    public function testInvalidUserBirthday()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/user/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "email" : "valid@email.com",
                "lname" : "test",
                "fname" : "test",
                "password" : "12345678",
                "birthday" : "20-10-2016"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"Age is under 13 !"}', $client->getResponse()->getContent());
    }

    public function  testCreateToDoListValidUser(){
        $client = static::createClient();
        $content = '
                   {
                       "userID" : "1"
                   }';
        $client->request(
            'POST',
            '/api/ToDoList/create',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $content
        );
        $this->toDoList =  json_decode($client->getResponse()->getContent(),true)['id'];
        $this->assertResponseStatusCodeSame(201, $client->getResponse()->getStatusCode());
        $this->assertContains('{"status":"ok"', $client->getResponse()->getContent());
    }

    public function testAddItem()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/ToDoList/addItem',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "name" : "valid",
                "content" : "valid",
                "toDoListID" : "1"
            }'
        );
        $this->assertResponseStatusCodeSame(201, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"status":"ok"}', $client->getResponse()->getContent());

    }

    public function testAddItemInvalid()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/ToDoList/addItem',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "name" : "",
                "content" : "valid",
                "toDoListID" : "1"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"Item null or invalid"}', $client->getResponse()->getContent());
    }

    public function testAddItemTooRecent()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/ToDoList/addItem',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "name" : "valid",
                "content" : "valid",
                "toDoListID" : "1"
            }'
        );
        $client->request(
            'POST',
            '/api/ToDoList/addItem',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            '
            {
                "name" : "valid",
                "content" : "valid",
                "toDoListID" : "1"
            }'
        );
        $this->assertResponseStatusCodeSame(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"error":"Last item is too recent"}', $client->getResponse()->getContent());
    }

}