<?php

namespace App\Tests;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use App\Entity\Item;


class ItemTest extends TestCase
{

    public function testItemIsValid(){
        $item = new Item("Item1","un objet",new Carbon("27-05-2020"));
        $this->assertEquals(true, $item->isValid());
    }

    public function testNameIsNotValid()
    {
        $item = new Item("","un objet",new Carbon("27-05-2020"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Name is not valid !");
        $this->assertEquals(Exception::class, $item->isValid());
    }

    public function testContentIsNotValid()
    {
        $item = new Item("Item1","",new Carbon("27-05-2020"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Content is not valid !");
        $this->assertEquals(Exception::class, $item->isValid());
    }

    public function testContentIsNotValid2()
    {
        $item = new Item("Item1","1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",new Carbon("27-05-2020"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Content must be less than 1000 characters !");
        $this->assertEquals(Exception::class, $item->isValid());
    }

    public function testDateIsNotValid()
    {
        $item = new Item("Item1","un Objet",new Carbon("27-05-2021"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Error, date is in the future !");
        $this->assertEquals(Exception::class, $item->isValid());
    }

}

