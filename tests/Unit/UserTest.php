<?php

namespace App\Tests;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use App\Entity\User;


class UserTest extends TestCase
{

    public function testUserIsValid(){
        $user = new User("test@gmail.com","nom","prenom",'12345678',new Carbon("27-06-1996"));
        $this->assertEquals(true, $user->isValid());
    }

    public function testLastnameIsNotValid()
    {
        $user = new User("test@gmail.com","","prenom",'12345678',new Carbon("27-06-1996"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Last Name is not valid !");
        $this->assertEquals(Exception::class, $user->isValid());
    }

    public function testFirstnameIsNotValid()
    {
        $user = new User("test@gmail.com","nom","",'12345678',new Carbon("27-06-1996"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("First Name is not valid !");
        $this->assertEquals(Exception::class, $user->isValid());
    }

    public function testPasswordIsNotValid()
    {
        $user = new User("test@gmail.com","nom","prenom",'12345',new Carbon("27-06-1996"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Password must be greater than 8 characters !");
        $this->assertEquals(Exception::class, $user->isValid());
    }

    public function testPasswordIsNotValid2()
    {
        $user = new User("test@gmail.com","nom","prenom",'123456789101234567891012345678910123456789101',new Carbon("27-06-1996"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Password must be less than 40 characters !");
        $this->assertEquals(Exception::class, $user->isValid());
    }

    public function testBirthdayIsNotValid()
    {
        $user = new User("test@gmail.com","nom","prenom",'12345678',new Carbon("27-06-2016"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Age is under 13 !");
        $this->assertEquals(Exception::class, $user->isValid());
    }

    public function testMailIsNotValid()
    {
        $user = new User("testgmail.com","nom","prenom",'12345678',new Carbon("27-06-1996"));
        $this->expectException('Exception');
        $this->expectExceptionMessage("Mail is not Valid !");
        $this->assertEquals(Exception::class, $user->isValid());
    }



}

