<?php

namespace App\Tests;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Entity\Item;
use Exception;



class ToDoListTest extends TestCase
{
    private $user;
    private $todolist;
    private $item;

    public function setUp(){
        parent::setUp();

        $this->user = new User(
            'test@test.fr',
            'test',
            'test',
            '12345678',
            new Carbon("27-06-1996")
        );
        $this->item = new Item(
            'test',
            'test',
            new Carbon('27-12-2020')
        );
        $this->todolist = $this->getMockBuilder(ToDoList::class)->setMethods(['getLastItem','itemsCount'])->getMock();
        $this->todolist->setOwner($this->user);}


    public function testCanAddItemValid(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(2);
        $canAddItem = $this->todolist->canAddItem($this->item);
        $this->assertTrue($canAddItem);
    }

    public function testCanAddItemInvalidUser(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(2);
        $this->user->setEmail("invalid");
        $this->expectException('Exception');
        $this->expectExceptionMessage('User null or invalid');
        $this->todolist->canAddItem($this->item);
    }

    public function testCanAddItemInvalidItem(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(2);
        $this->item->setName('');
        $this->expectException('Exception');
        $this->expectExceptionMessage('Item null or invalid');
        $this->todolist->canAddItem($this->item);
    }

    public function testCanAddItemTooRecent(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(2);
        $this->item->setCreatedAt(Carbon::now()->subMinutes(10));
        $this->expectException('Exception');
        $this->expectExceptionMessage('Last item is too recent');
        $this->todolist->canAddItem($this->item);
    }

    public function testCanAddItemFullList(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(10);
        $this->expectException('Exception');
        $this->expectExceptionMessage('List is full');
        $this->todolist->canAddItem($this->item);
    }

    public function testSendMail(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(8);
        $stub = $this->createMock(User::class);
        $this->todolist->setOwner($stub);
        $stub->method('sendMail')
            ->willReturn(true);
        $addItem = $this->todolist->addItem($this->item);
        $this->assertEquals($addItem,$this->todolist);
    }

    public function testSendMailError(){
        $this->todolist->expects($this->any())->method('getLastItem')->willReturn($this->item);
        $this->todolist->expects($this->any())->method('itemsCount')->willReturn(8);
        $stub = $this->createMock(User::class);
        $stub->method('sendMail')
            ->willReturn(false);
        $stub->method('isValid')
            ->willReturn(true);
        $this->todolist->setOwner($stub);
        $this->expectException('Exception');
        $this->expectExceptionMessage('Unable to send email');
        $this->todolist->addItem($this->item);
    }

}
