<?php
namespace App\Controller;
use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Repository\ItemRepository;
use App\Repository\ToDoListRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Exception;
use Symfony\Component\Routing\Annotation\Route;


/**
 *
 * @Route("/api", name="api_")
 */
class ApiController extends AbstractFOSRestController
{

    /**
     *
     * @Rest\Get("/users")
     *
     * @return Response
     */
    public function usersAction()
    {

        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->handleView($this->view($users));
    }

    /**
     *
     * @Rest\Get("/lists")
     *
     * @return Response
     */
    public function listsAction()
    {
        $lists = $this->getDoctrine()->getRepository(ToDoList::class)->findAll();
        return $this->handleView($this->view($lists));
    }

    /**
     *
     * @Rest\Post("/user/create")
     *
     * @return Response
     */
    public function createUserAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = new User($data["email"], $data['lname'] ,$data["fname"],$data["password"],new Carbon($data["birthday"]));

        try{
            $user->isValid();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->handleView($this->view(['status' => 'ok', "id" => "".$user->getId()], Response::HTTP_CREATED));
        }catch (Exception $e){
            return $this->handleView($this->view(['error' => $e->getMessage()], Response::HTTP_NOT_ACCEPTABLE));
        }
    }


    /**
     *
     * @Rest\Post("/ToDoList/create")
     *
     * @return Response
     */
    public function createToDoListaction(Request $request, UserRepository $repository)
    {
        $data = json_decode($request->getContent(), true);
        $todolist = new ToDoList();
        $user = $repository->findOneBy(["id" => "".$data["userID"]]);
        try{
            $todolist->setOwner($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todolist);
            $entityManager->flush();
            return $this->handleView($this->view(['status' => 'ok', 'id' => "".$todolist->getId()], Response::HTTP_CREATED));
        }catch (Exception $e){
            return $this->handleView($this->view(['error' => $e->getMessage()], Response::HTTP_NOT_ACCEPTABLE));
        }
    }

    /**
     *
     * @Rest\Post("/ToDoList/addItem")
     *
     * @return Response
     */
    public function addItemAction (Request $request, ToDoListRepository $repositoryList, ItemRepository $repositoryItem)
    {
        $data = json_decode($request->getContent(), true);
        $todolist = $repositoryList->findOneBy(["id" => $data["toDoListID"]]);
        $item = new Item($data["name"], $data["content"], Carbon::now());

        try{
            $todolist->canAddItem($item);
            $todolist->addItem($item);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($todolist);
            $entityManager->flush();
            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }catch (Exception $e){
            return $this->handleView($this->view(['error' => $e->getMessage()], Response::HTTP_NOT_ACCEPTABLE));
        }
    }

}

