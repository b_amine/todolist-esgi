<?php

namespace App\Entity;

use Carbon\Carbon;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Exception;
/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */

class User{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="datetime")
     */
    private $birthday;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFname(): ?string
    {
        return $this->fname;
    }

    public function setFname(string $fname): self
    {
        $this->fname = $fname;

        return $this;
    }

    public function getLname(): ?string
    {
        return $this->lname;
    }

    public function setLname(string $lname): self
    {
        $this->lname = $lname;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBirthday(): Carbon
    {
        return $this->birthday;
    }

    public function setBirthday(Carbon $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }
    /**
     * @ORM\OneToOne(targetEntity=ToDoList::class, mappedBy="owner", cascade={"persist", "remove"})
     */
    private $todolist;

    public function __construct($email, $lname, $fname,$password, $birthday){
        $this->email = $email;
        $this->email = $email;
        $this->lname = $lname;
        $this->fname = $fname;
        $this->password = $password;
        $this->birthday = $birthday;
    }

    public function isValid() : bool {
        $age = $this->birthday
            ->diff(new Carbon('now'))
            ->y;

        if (empty($this->lname)){
            throw new Exception("Last Name is not valid !");
        }
        if (empty($this->fname)){
            throw new Exception("First Name is not valid !");
        }
        if (strlen($this -> password)>40){
            throw new Exception("Password must be less than 40 characters !");
        }
        if (strlen($this -> password)<8){
            throw new Exception("Password must be greater than 8 characters !");
        }
        if ($age < 13) {
            throw new Exception("Age is under 13 !");
        }
        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL) ) {
            throw new Exception("Mail is not Valid !");
        }

        return true;

    }

    public function getTodolist(): ?ToDoList
    {
        return $this->todolist;
    }

    public function setTodolist(ToDoList $todolist): self
    {
        // set the owning side of the relation if necessary
        if ($todolist->getOwner() !== $this) {
            $todolist->setOwner($this);
        }

        $this->todolist = $todolist;

        return $this;
    }
    public function sendMail() : bool {
        $destinataire = $this->email;
        $expediteur = $this->email;
        $objet = 'Attention !'; // Objet du message
        $headers = 'From: "Nom_de_expediteur"<'.$expediteur.'>'."\n"; // Expediteur
        $headers .= 'Delivered-to: '.$destinataire."\n"; // Destinataire
        $message = "Plus que 2 item à ajouter !";
        if (mail($destinataire, $objet, $message, $headers)) // Envoi du message
        {
            return true;
        }
        else // Non envoyé
        {
            return false;
        }

    }


}
