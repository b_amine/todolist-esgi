<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Exception;


/**
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=ToDoList::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
    private $todolist;

    public function __construct($name, $content, $createdAt){
        $this->name = $name;
        $this->content = $content;
        $this->createdAt = $createdAt;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTodolist(): ?ToDoList
    {
        return $this->todolist;
    }

    public function setTodolist(?ToDoList $todolist): self
    {
        $this->todolist = $todolist;

        return $this;
    }

    public function isValid() : bool {
        if (empty($this->name)){
            throw new Exception("Name is not valid !");
        }
        if (empty($this->content)){
            throw new Exception("Content is not valid !");
        }
        if (strlen($this->getContent()) >= 1000){
            throw new Exception("Content must be less than 1000 characters !");
        }
        if (Carbon::now()->lt($this->createdAt->toDateTimeString())) { //La date de création d'un item ne peut pas etre une date dans le futur
            throw new Exception("Error, date is in the future !");
        }
        return true;

    }

}
