<?php

namespace App\Entity;

use App\Repository\ToDoListRepository;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use phpDocumentor\Reflection\Types\This;

/**
 * @ORM\Entity(repositoryClass=ToDoListRepository::class)
 */
class ToDoList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="todolist", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Item::class, mappedBy="todolist", orphanRemoval=true, cascade={"persist"})
     */
    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if($this->canAddItem($item)) {
            if (!$this->items->contains($item)) {
                $this->items[] = $item;
                $item->setTodolist($this);
            }
            if($this->itemsCount() == 8){
                if(!$this->getOwner()->sendMail()){
                    throw new Exception("Unable to send email");
                }
            }
        }
        return $this;

    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getTodolist() === $this) {
                $item->setTodolist(null);
            }
        }

        return $this;
    }

    public function canAddItem(Item $item){

        try {
            $item->isValid();
        }
        catch (Exception $e) {
            throw new Exception("Item null or invalid");
        }
        if(is_null($item)){
            throw new Exception("Item null or invalid");
        }
        if(is_null($this->getOwner())){
            throw new Exception("User null or invalid");
        }
        try {
            $this->getOwner()->isValid();
        }
        catch (Exception $e) {
            throw new Exception("User null or invalid");
        }
        if($this->itemsCount() >= 10){
            throw new Exception("List is full");
        }
        if($this->itemsCount() > 0) {
            if (Carbon::now()->subMinutes(30)->isBefore($this->getLastItem()->getCreatedAt())) {
                throw new Exception("Last item is too recent");
            }
        }
        return true;
    }

    public function getLastItem(){
        return $this->getItems()->first();
    }

    protected function itemsCount(){
        return sizeof($this->getItems());
    }
}
