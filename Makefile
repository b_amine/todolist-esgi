unit:
	php bin/phpunit tests/Unit

integration:
	bash -x scripts/integration-tests.sh

empty-db :
	bash -x scripts/empty-db.sh

all-tests: unit integration
